..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

######################################################
Welcome to Squash AUTOM & Squash DEVOPS documentation!
######################################################

|squashAutom| is a set of components for the management of your automated tests' executions.

|squashDevops| is a set of components for the integration to your continuous integration pipeline
of your automated functional tests' executions.

.. toctree::
   :maxdepth: 2

   Squash AUTOM<autom/autom.rst>
   Squash DEVOPS<devops/devops.rst>
   FAQ<faq/faq.rst>

|

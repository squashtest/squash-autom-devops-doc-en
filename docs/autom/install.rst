..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



##################
Installation Guide
##################

.. contents::
   :local:
   :depth: 1

|

.. _squash_orchestrator_installation:

*******************
Squash Orchestrator
*******************

Overview
========

|squashOrchestrator| enables you to run and coordinate the various components of the test execution chain
(execution environments, automata, reporting, etc.).
It is based on the OpenTestFactory orchestrator and add a set of micro-services to extend its possibilities,
such as running Squash TM execution plan or reporting to Squash TM.

Installation
============

|squashOrchestrator| is a set of services running together. They may or may not run on the same machine,
and they may or may not be started at the same time.

The only prerequisite is that the *EventBus*, the service they use to communicate together, is available when they launch.

To facilitate the installation of the orchestrator, an ‘all-in-one’ docker image is provided. It contains all core services and
Squash specific services.

To get the latest image of |squashOrchestrator|, use the following command:

.. code-block:: bash

 docker pull squashtest/squash-orchestrator:latest

Usage
=====

Configuring the ‘all-in-one’ image
----------------------------------

The execution of the following command will start the |squashOrchestrator| using an existing execution environment,
with self-generated trusted keys (which is not recommended in a production setup):

.. code-block:: bash

  docker run -d \
           --name orchestrator \
           -p 7774:7774 \
           -p 7775:7775 \
           -p 7776:7776 \
           -p 38368:38368 \
           -e SSH_CHANNEL_HOST=the_environment_ip_or_hostname \
           -e SSH_CHANNEL_USER=user \
           -e SSH_CHANNEL_PASSWORD=secret \
           -e SSH_CHANNEL_TAGS=ssh,linux,robotframework \
            squashtest/squash-orchestrator:latest

It exposes the following services on the corresponding ports:

* *receptionnist* (port 7774)
* *observer* (port 7775)
* *killswitch* (port 7776)
* *eventbus* (port 38368)

For more details about Squash Orchestrator configuration, please check the `OpenTestFactory orchestrator documentation
<https://opentestfactory.github.io/orchestrator/installation/>`_ on which |squashOrchestrator| is based on.

|

*****************************************************
Docker image of Squash AUTOM exclusive micro-services
*****************************************************

Overview
========

**Squash AUTOM Premium** license gives you access to a Docker image with |squashOrchestrator| micro-services offering
the following functionalities:

* Agilitest test execution management
* Ranorex test execution management
* UFT test execution management

Installation
============

To install the Docker image of Squash AUTOM exclusives micro-services, you must get the compressed image from Squash
support service. Then you have to execute the following command:

.. code-block:: bash

 docker load -i squash-autom-premium-2.0.0.tar.gz

Usage
=====

To run the Docker image of Squash AUTOM exclusives micro-services, the following command must be used:


.. code-block:: bash

 docker run -e BUS_HOST=<IP or DNS name of event bus> \
            -e BUS_PORT=<port> -e BUS_TOKEN=<JWT token> \
            --volume /path/to/truststore:/etc/squashtf/ \
            docker.squashtest.org/squashtest/squash-autom-premium:2.0.0

with :

* *BUS_HOST* (mandatory) : IP address or DNS name of *EventBus* service of the |squashOrchestrator| with
  which micro-services communicate.
* *BUS_PORT* (mandatory) : port of *EventBus* service of the |squashOrchestrator| with
  which micro-services communicate.
* *BUS_TOKEN* (optionnal) : JWT token accepted by the *EventBus* service of the |squashOrchestrator| with
  which micro-services communicate.
* *--volume* : create a volume to a truststore with trusted public keys that can be used to validate JWT tokens.
  The volume must be created if JWT token validation is required to exchange messages between micro-services.

|

*********************
OpenTestFactory Agent
*********************

Overview
============

OpenTestFactory agent is a process that runs on an execution environment. This process contacts the |squashOrchestrator|
at regular intervals, looking for orders to execute. Whenever there is a pending order, the agent will do as asked
and send the result back to the orchestrator.

Installation
============

The OpenTestFactory agent is a Python application that is installed in the execution environment.
It requires Python 3.8 or higher. It works on Linux, MacOS, and Windows.

The agent is a simple script. It only has one external dependency, the well known ``requests`` library
(it will be installed if not already present on the execution environment).

To install it, use the following command:

.. code-block:: bash

  pip3 install --upgrade opentf-agent

You can test your installation by running the following command:

.. code-block:: bash

  opentf-agent --help

Usage
=====

Summary
-------

.. code-block:: bash

  $ opentf-agent --help
  usage: opentf-agent [-h] --tags TAGS --host HOST [--port PORT] [--path_prefix PATH_PREFIX] [--token TOKEN] [--encoding ENCODING] [--script_path SCRIPT_PATH]
                      [--workspace_dir WORKSPACE_DIR] [--name NAME] [--polling_delay POLLING_DELAY] [--liveness_probe LIVENESS_PROBE] [--retry RETRY] [--debug]

  OpenTestFactory Agent

  optional arguments:
    -h, --help            show this help message and exit
    --tags TAGS           a comma-separated list of tags (e.g. windows,robotframework)
    --host HOST           target host with protocol (e.g. https://example.local)
    --port PORT           target port (default to 24368)
    --path_prefix PATH_PREFIX
                        target context path (default to no context path)
    --token TOKEN         token
    --encoding ENCODING   encoding on the console side (defaults to utf-8)
    --script_path SCRIPT_PATH
                        where to put temporary files (defaults to current directory)
    --workspace_dir WORKSPACE_DIR
                        where to put workspaces (defaults to current directory)
    --name NAME           agent name (defaults to "test agent")
    --polling_delay POLLING_DELAY
                        polling delay in seconds (default to 5)
    --liveness_probe LIVENESS_PROBE
                        liveness probe in seconds (default to 300 seconds)
    --retry RETRY         how many time to try joining host (default to 5,
                        0 = try forever)
    --debug               whether to log debug informations.

Example
-------

Assuming there is a |squashOrchestrator| running on  ``orchestrator.example.com``, with a known token stored in the
``TOKEN`` environment variable, the following command will register the Windows-based execution environment
and will possibly receive commands from |squashOrchestrator| targeting windows and/or robotframework tagged environments:

.. code-block:: bash

  chcp 65001
  opentf-agent --tags windows,robotframework --host https://orchestrator.example.com/ --token %TOKEN%

The agent will poll the orchestrator every 5 seconds, and will execute the received commands.
The ``chcp`` command sets the console to Unicode. It is Windows-specific. It is not mandatory but may be needed
depending on the test framework available in the execution environment.

|

*************************************
Result Publisher Plugin for Squash TM
*************************************

Overview
============

The plugin is used simultaneously with |squashOrchestrator| to allow reporting to |squashTM| at the end of a |squashTM| automated test
plan execution by |squashOrchestrator|.

The plugin exists in a **Community** version (*squash.tm.rest.result.publisher.community-1.0.0.RELEASE.jar*) freely
available, or a **Premium** version (*squash.tm.rest.result.publisher.premium-1.0.0.RELEASE.jar*) available on request.

Installation
============

For details on the installation, please refer to the installation protocol of a |squashTM| plugin
(https://sites.google.com/a/henix.fr/wiki-squash-tm/installation-and-exploitation-guide/2---installation-of-squash-tm/7---jira-plug-in).

.. warning:: This plugin is compatible with version *1.22.2.RELEASE* or higher of |squashTM|.

|

*************************************
Squash AUTOM Plugin for Squash TM
*************************************

Overview
============

The plugin is used simultaneously with |squashOrchestrator| to allow automated test execution launch from |squashTM|.

The plugin exists in a **Community** version (*plugin.testautomation.squashautom.community-1.0.0.RELEASE.jar*) freely
available, or a **Premium** version (*plugin.testautomation.squashautom.premium-1.0.0.RELEASE.jar*) available on request.

Installation
============

For details on the installation, please refer to the installation protocol of a |squashTM| plugin
(https://sites.google.com/a/henix.fr/wiki-squash-tm/installation-and-exploitation-guide/2---installation-of-squash-tm/7---jira-plug-in).

.. warning:: This plugin is compatible with version *1.22.2.RELEASE* or higher of |squashTM|.

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##################################################
Piloting automated tests executions from Squash TM
##################################################

.. contents::
   :local:
   :depth: 1

|

.. note:: To pilote automated tests executions from Squash TM, following components are required:

          * Squash TM
          * Squash Orchestrator
          * Result Publisher Plugin for Squash TM
          * Squash AUTOM plugin for Squash TM
          * In case of *Agilitest* test execution : an *OpenTestFactory* agent for *Agilitest* execution environment
          * In case of *Ranorex* test execution : an *OpenTestFactory* agent for *Ranorex* execution environment, and an environment
            variable called *SQUASH_MSBUILD_PATH* containing the path to the parent folder of *MSBuild.exe*
          * In case of *UFT* test execution : an *OpenTestFactory* agent for *UFT* execution environment

.. _automate_with_squash:

******************************
Squash TM test case automation
******************************

.. note:: This page describes the common operations to all supported test frameworks in this version. You can access
          the automation specificities for each technology directly with the following links |_| :

          * :ref:`Agilitest<automate_with_agilitest>`
          * :ref:`Cucumber<automate_with_cucumber>`
          * :ref:`Cypress<automate_with_cypress>`
          * :ref:`JUnit<automate_with_junit>`
          * :ref:`Ranorex<automate_with_ranorex>`
          * :ref:`Robot Framework<automate_with_robot>`
          * :ref:`SoapUI<automate_with_soapui>`
          * :ref:`SKF<automate_with_skf>`
          * :ref:`UFT<automate_with_uft>`

Without using the Squash automation workflow
============================================

To execute a test case using the |squashOrchestrator|, its *Automation* panel in the *Information* tab of the test
case page must be correctly filled |_| :

    .. container:: image-container

        .. image:: ../_static/autom/tc-automated-space.png

* ``Automated test technology`` : A dropdown list allowing you to choose the technology used for the execution of a
  test case. In this version, only *Robot Framework*, *Junit*, *Cucumber*, *Cypress*, *SoapUi*, *SKF*, *Agilitest*, *Ranorex*
  and *UFT* are functioning.

* ``Source code repository URL`` : The address of the source code repository where the project is located, as referenced
  in the *Source code management servers* area of the *Administration*.

* ``Automated test reference`` : This is the location of the automated test within the project. This reference must
  follow the specific format of the used test technology (see :ref:`here<automation_specifics>`).

.. note:: *Agilitest*, *Ranorex* and *UFT* are only supported by the *premium* version of |squashAutom|.

--------------------

Using the Squash automation workflow
====================================

Regular test case
-----------------

To execute a test case using the |squashOrchestrator|, it must be automated in the *Automation Workspace* by filling
three columns |_| :

    .. container:: image-container

        .. image:: ../_static/autom/autom-workspace-columns.png

* ``Auto. test tech.`` : A dropdown list allowing you to choose the technology used for the execution of a
  test case. In this version, only *Robot Framework*, *Junit*, *Cucumber*, *Cypress*, *SKF*, *SoapUi*, *Agilitest*, *Ranorex*
  and *UFT* are functioning.

* ``Scm URL`` : The address of the source code repository where the project is located.

* ``Auto. test ref.`` : This is the location of the automated test within the project. This reference must
  follow the specific format of the used test technology (see :ref:`here<automation_specifics>`).

.. note:: *Agilitest*, *Ranorex* and *UFT* are only supported by the *premium* version of |squashAutom|.

BDD or Gherkin test case
------------------------

The information of the *Automation* panel is automatically filled during the transmission of a BDD or Gherkin script
to a remote source code repository hosting service. It can also be modified by the user at any moment.

--------------------

.. _squash_params:

Squash TM parameters exploitation
=================================

When a |squashTM| execution plan is launched (through an EPAC or directly from the campaign workspace), |squashTM| will
transmit various information on ITPI that can be exploited by a *Cucumber*, *Cypress*, *Robot Framework*, *SKF*, *Agilitest*,
*Ranorex* or *UFT* test case. Details of this functionality can be found on the corresponding used technology section

--------------------

.. _automation_specifics:

Automation frameworks specifics
===============================

.. _automate_with_agilitest:

Automation with Agilitest
-------------------------

1. Test reference
^^^^^^^^^^^^^^^^^

In order to bind a |squashTM| test case to an *Agilitest* automated test, the content of the *Automated test
reference* field of the *Automation* panel of a test case must have the following format |_| :

``[1] / [2]``

With :

* ``[1]`` : Name of the project on the source code repository.

* ``[2]`` : Path and name of the *ActionTestScript* file, from the root of the project (with the ``.ats`` extension).

.. warning:: The ATS script **must** be located in ``src/main/ats/*`` , as in any regular ATS project architecture.


2. Nature of the exploitable Squash TM parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The exploitable |squashTM| parameters in an *ActionTestScript* script will differ depending on whether you're using the
**Community** or **Premium** version of |squashDevops|.

Here is a table showing the exploitable parameters |_| :

.. csv-table::
   :header: "Nature","Key","Community","Premium"
   :align: center

   "Name of the dataset","DSNAME",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Dataset parameter","DS_[name]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Test case reference","TC_REF",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Test case CUF","TC_CUF_[code]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Iteration CUF","IT_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "Campaign CUF","CPG_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "Test suite CUF","TS_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg

*Legend :*

* ``CUF`` : *Custom Field*
* ``[code]`` : *Value of a CUF's "Code" field*
* ``[name]`` : *Parameter name as filled in Squash TM*

3. Squash TM parameters usage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When executing a |squashTM| automated test case with *Agilitest*, it is possible to exploit the |squashTM| parameters inside
the test.

In order to achieve this, you'll have to follow these steps |_| :

* Create custom fields in |squashTM| and bind them to the project bearing the test plan to execute.

* Make sure that the *code* fields of the parameters correspond to the names of the existing environment variables
  present in the *Cypress* script.

.. note:: |squashTM| adds a prefix to the *code* of the transmitted custom field. Make sure to take it into account.
          Please refer to the |squashTM|
          `documentation <https://sites.google.com/a/henix.fr/wiki-squash-tm/administrator-guide/05---custom-field-administration>`_
          for more information.

|

Below is an example of an *Agilitest* test file and the corresponding |squashTM| test case automation :

    .. container:: image-container

        .. image:: ../_static/autom/agilitest-test-with-params-1.png

    .. container:: image-container

        .. image:: ../_static/autom/agilitest-test-with-params-2.png

|

--------------------

.. _automate_with_cucumber:

Automation with Cucumber
------------------------

1. Test reference
^^^^^^^^^^^^^^^^^

.. note:: If a scenario name is not specified, the result of each executed |squashTM| test case is calculated
          by taking into account the individual results of each scenario included in the bound file |_| :

          * If at least one scenario has an *Error* status (in case of a technical issue), the status of the execution
            will be *Blocked*.
          * If at least one scenario fails functionally and none of the other has an *Error* status, the status of the
            execution will be *Failed*.
          * If all scenarios succeed, the status of the execution will be *Success*.

In order to bind a |squashTM| test case to a *Cucumber* automated test, the content of the *Automated test
reference* field of the *Automation* panel of a test case must have the following format |_| :

``[1] / [2] # [3] # [4]``

With :

* ``[1]`` : Name of the project on the source code repository.

* ``[2]`` : Path and name of the *Cucumber* test file, from the root of the project (with the ``.feature`` extension).

* ``[3]`` : feature name as specified in the *Cucumber* test file.

* ``[4]`` : scenario name as specified in the *Cucumber* test file.

2. Nature of the exploitable Squash TM parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Squash AUTOM** and **Squash DEVOPS** are able to use the name of a |squashTM| dataset as a tag value to use
for the execution of a specific subset of a *Cucumber* feature.

Both **Community** and **Premium** versions can use dataset names.

3. Squash TM parameters usage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When executing a |squashTM| automated test case with *Cucumber*, it is possible to exploit the |squashTM| dataset name
in order to execute a specific dataset of a *Cucumber* scenario.

In order to achieve this, you'll have to follow these steps |_| :

* Fill the datasets in the *Parameters* tab of the test case in |squashTM|.

* Create in a *Cucumber* scenario as many example table as there are dataset in |squashTM| test case. Annotate them with
  a tag corresponding to the name of a |squashTM| dataset.

* Create one line of elements in each example table to set scenario's parameters values for the dataset.

|

Below is an example of a *Cucumber* test file and the corresponding |squashTM| test case automation :

    .. container:: image-container

        .. image:: ../_static/autom/cucumber-test-with-tags-1.png

    .. container:: image-container

        .. image:: ../_static/autom/cucumber-test-with-tags-2.png

    .. container:: image-container

        .. image:: ../_static/autom/cucumber-test-with-tags-3.png

|

--------------------

.. _automate_with_cypress:

Automation with Cypress
-----------------------

1. Test reference
^^^^^^^^^^^^^^^^^

.. note:: In this version of |squashAutom|, it is not possible to select a specific scenario in a ``.spec.js`` file
          containing several ones |_| : all scenarios in the file are therefore executed together. The result of each
          executed |squashTM| test case is calculated by taking into account the individual results of each scenario
          included in the bound file |_| :

          * If at least one scenario has an *Error* status (in case of a technical issue), the status of the execution
            will be *Blocked*.
          * If at least one scenario fails functionally and none of the other has an *Error* status, the status of the
            execution will be *Failed*.
          * If all scenarios succeed, the status of the execution will be *Success*.

In order to bind a |squashTM| test case to a *Cypress* automated test, the content of the *Automated test
reference* field of the *Automation* panel of a test case must have the following format |_| :

``[1] / [2]``

With :

* ``[1]`` : Name of the project on the source code repository.

* ``[2]`` : Path and name of the *Cypress* test file, from the root of the project (with the ``.spec.js`` extension).

2. Nature of the exploitable Squash TM parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The exploitable |squashTM| parameters in a *Cypress* script will differ depending on whether you're using the **Community**
or **Premium** version of |squashDevops|.

Here is a table showing the exploitable parameters |_| :

.. csv-table::
   :header: "Nature","Key","Community","Premium"
   :align: center

   "Name of the dataset","DSNAME",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Dataset parameter","DS_[name]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Test case reference","TC_REF",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Test case CUF","TC_CUF_[code]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Iteration CUF","IT_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "Campaign CUF","CPG_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "Test suite CUF","TS_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg

*Legend :*

* ``CUF`` : *Custom Field*
* ``[code]`` : *Value of a CUF's "Code" field*
* ``[name]`` : *Parameter name as filled in Squash TM*

3. Squash TM parameters usage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When executing a |squashTM| automated test case with *Cypress*, it is possible to exploit the |squashTM| parameters inside
the test.

In order to achieve this, you'll have to follow these steps |_| :

* Create custom fields in |squashTM| and bind them to the project bearing the test plan to execute.

* Make sure that the *code* fields of the parameters correspond to the names of the existing environment variables
  present in the *Cypress* script.

.. note:: |squashTM| adds a prefix to the *code* of the transmitted custom field. Make sure to take it into account.
          Please refer to the |squashTM|
          `documentation <https://sites.google.com/a/henix.fr/wiki-squash-tm/administrator-guide/05---custom-field-administration>`_
          for more information.

|

Below is an example of a *Cypress* test file and the corresponding |squashTM| test case automation :

    .. container:: image-container

        .. image:: ../_static/autom/cypress-test-with-params-1.png

    .. container:: image-container

        .. image:: ../_static/autom/cypress-test-with-params-2.png

|

--------------------

.. _automate_with_junit:

Automation with JUnit
-------------------------

Test reference
^^^^^^^^^^^^^^

In order to bind a |squashTM| test case to a *JUnit* automated test, the content of the *Automated test
reference* field of the *Automation* panel of a test case must have the following format |_| :

``[1] / [2] # [3]``

With :

* ``[1]`` : Name of the project on the source code repository.

* ``[2]`` : Qualified name of the test class.

* ``[3]`` : Name of the method to test in the test class.

Below is an example of a test class and the corresponding |squashTM| test case automation :

    .. container:: image-container

        .. image:: ../_static/autom/junit-test-with-param-1.png

    .. container:: image-container

        .. image:: ../_static/autom/junit-test-with-param-2.png

|

--------------------

.. _automate_with_ranorex:

Automation with Ranorex
-----------------------

This feature is available only in the *Premium* version of |squashAutom|.

In order to use this feature, an *OpenTestFactory* agent for *Ranorex* execution environment is needed. Furthermore, an environment
variable called **SQUASH_MSBUILD_PATH** containing the path to the parent folder of **MSBuild.exe** must be created.
You can execute the following command to get the path to your *MSBuild* executable |_| :

.. code-block:: console

   reg.exe query "HKLM\SOFTWARE\Microsoft\MSBuild\ToolsVersions\4.0" /v MSBuildToolsPath

1. Test reference
^^^^^^^^^^^^^^^^^

In order to bind a |squashTM| test case to a *Ranorex* automated test, the content of the *Automated test
reference* field of the *Automation* panel of a test case must have the following format |_| :

``[1] # [2] # [3] # [4]``

With :

* ``[1]`` : The path to the solution's ``.sln`` file from the root of the project source repository.

* ``[2]`` : *Ranorex* project name to execute.

* ``[3]`` : *Ranorex* test suite name to execute.

* ``[4]`` : *Ranorex* test case name to execute. This parameter is optional.

2. Nature of the exploitable Squash TM parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here is a table showing the exploitable parameters |_| :

.. csv-table::
   :header: "Nature","Key"
   :align: center

   "Name of the dataset","DSNAME"
   "Dataset parameter","DS_[name]"
   "Test case reference","TC_REF"
   "Test case CUF","TC_CUF_[code]"
   "Iteration CUF","IT_CUF_[code]"
   "Campaign CUF","CPG_CUF_[code]"
   "Test suite CUF","TS_CUF_[code]"

*Legend :*

* ``CUF`` : *Custom Field*
* ``[code]`` : *Value of a CUF's "Code" field*
* ``[name]`` : *Parameter name as filled in Squash TM*

3. Squash TM parameters usage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When executing a |squashTM| automated test case with *Ranorex*, it is possible to exploit the |squashTM| parameters inside
the test.

In order to achieve this, you'll have to follow these steps |_| :

* Create custom fields in |squashTM| and bind them to the project bearing the test plan to execute.

* Make sure that the *code* fields of the parameters correspond to the names of the existing parameters
  present in the *Ranorex* solution.

.. note:: |squashTM| adds a prefix to the *code* of the transmitted custom field. Make sure to take it into account.
          Please refer to the |squashTM|
          `documentation <https://sites.google.com/a/henix.fr/wiki-squash-tm/administrator-guide/05---custom-field-administration>`_
          for more information.

|

Below is an example of a *Ranorex* solution and the corresponding |squashTM| test case automation :

    .. container:: image-container

        .. image:: ../_static/autom/ranorex-test-with-param-1.png

    .. container:: image-container

        .. image:: ../_static/autom/ranorex-test-with-param-2.png

|

--------------------

.. _automate_with_robot:

Automation with Robot Framework
-------------------------------

1. Test reference
^^^^^^^^^^^^^^^^^

In order to bind a |squashTM| test case to a *Robot Framework* automated test, the content of the *Automated test
reference* field of the *Automation* panel of a test case must have the following format |_| :

``[1] / [2] # [3]``

With :

* ``[1]`` : Name of the project on the source code repository.

* ``[2]`` : Path and name of the *Robot Framework* test, from the root of the project (with the ``.robot`` extension).

* ``[3]`` : Name of the test case to execute in the ``.robot`` file.

2. Nature of the exploitable Squash TM parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The exploitable |squashTM| parameters in a *Robot Framework* script will differ depending on whether you're using the
**Community** or **Premium** version of |squashDevops|.

Here is a table showing the exploitable parameters |_| :

.. csv-table::
   :header: "Nature","Key","Community","Premium"
   :align: center

   "Name of the dataset","DSNAME",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Dataset parameter","DS_[name]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Test case reference","TC_REF",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Test case CUF","TC_CUF_[code]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Iteration CUF","IT_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "Campaign CUF","CPG_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "Test suite CUF","TS_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg

*Legend :*

* ``CUF`` : *Custom Field*
* ``[code]`` : *Value of a CUF's "Code" field*
* ``[name]`` : *Parameter name as filled in Squash TM*

3. Squash TM parameters usage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When executing a |squashTM| automated test case with *Robot Framework*, it is possible to exploit the |squashTM|
parameters inside the test.

In order to achieve this, you'll have to follow these steps |_| :

* Create custom fields in |squashTM| and bind them to the project bearing the test plan to execute.

* Install the *squash-tf-services* python library on the environment where the *Robot Framework* execution takes place.
  It is accessible through the ``pip`` package management and can be installed by executing the following command line |_| :

.. code-block:: bash

   python -m pip install squash-tf-services

* Import the library inside the ``.robot`` file in the *Settings* section |_| :

.. code-block:: bash

    Library squash_tf.TFParamService

* You can then retrieve the value of a |squashTM| parameter by calling the following keyword |_| :

.. code-block:: bash

    Get Param <parameter key>

|

Below is an example of a *Robot Framework* test file and the corresponding |squashTM| test case automation :

    .. container:: image-container

        .. image:: ../_static/autom/robot-test-with-param-1.png

    .. container:: image-container

        .. image:: ../_static/autom/robot-test-with-param-2.png

|

--------------------

.. _automate_with_skf:

Automation with SKF
-----------------------

1. Test reference
^^^^^^^^^^^^^^^^^

In order to bind a |squashTM| test case to a *SKF* automated test, the content of the *Automated test
reference* field of the *Automation* panel of a test case must have the following format |_| :

``[1] / [2] . [3] # [4]``

With :

* ``[1]`` : Path to the root skf folder (which contains the pom.xml file) on the source repository.

* ``[2]`` : Default test ecosystem of the skf project (tests).

* ``[3]`` : Child tests ecosystem (it is possible to add several by separating them by ``.``; this parameter is optional).

* ``[4]`` : Name of the test script to run (with its ``.ta`` extension; this parameter is optional, if it is not entered, all the test scripts of the target ecosystem will be executed)

2. Nature of the exploitable Squash TM parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The exploitable |squashTM| parameters in a *SKF* script will differ depending on whether you're using the
**Community** or **Premium** version of |squashDevops|.

Here is a table showing the exploitable parameters |_| :

.. csv-table::
   :header: "Nature","Key","Community","Premium"
   :align: center

   "Name of the dataset","DSNAME",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Dataset parameter","DS_[name]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Test case reference","TC_REF",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Test case CUF","TC_CUF_[code]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Iteration CUF","IT_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "Campaign CUF","CPG_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "Test suite CUF","TS_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg

*Legend :*

* ``CUF`` : *Custom Field*
* ``[code]`` : *Value of a CUF's "Code" field*
* ``[name]`` : *Parameter name as filled in Squash TM*

3. Squash TM parameters usage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When executing a |squashTM| automated test case with *SKF*, it is possible to exploit the |squashTM| parameters inside
the test.

In order to achieve this, you'll have to follow these steps |_| :

* Create custom fields in |squashTM| and bind them to the project bearing the test plan to execute.

* Make sure that the *code* fields of the parameters correspond to the names of the existing parameters
  present in the *SKF* project.

.. note:: |squashTM| adds a prefix to the *code* of the transmitted custom field. Make sure to take it into account.
          Please refer to the |squashTM|
          `documentation <https://sites.google.com/a/henix.fr/wiki-squash-tm/administrator-guide/05---custom-field-administration>`_
          for more information.

|

Below is an example of a *SKF* project and the corresponding |squashTM| test case automation :

    .. container:: image-container

        .. image:: ../_static/autom/skf-test-with-params-1.png

    .. container:: image-container

        .. image:: ../_static/autom/skf-test-with-params-2.png

--------------------

.. _automate_with_soapui:

Automation with SoapUI
----------------------

Test reference
^^^^^^^^^^^^^^

In order to bind a |squashTM| test case to a *SoapUI* automated test, the content of the *Automated test
reference* field of the *Automation* panel of a test case must have the following format |_| :

``[1] / [2] # [3] # [4]``

With :

* ``[1]`` : Name of the project on the source code repository.

* ``[2]`` : Path and name of the *SoapUI* test file, from the root of the project (with the ``.xml`` extension).

* ``[3]`` : Name of the TestSuite containing the test case.

* ``[4]`` : Name of the test case to execute.

Below is an example of a *SoapUI* test file and the corresponding |squashTM| test case automation :

    .. container:: image-container

        .. image:: ../_static/autom/soapui-test-1.png

    .. container:: image-container

        .. image:: ../_static/autom/soapui-test-2.png

|

--------------------

.. _automate_with_uft:

Automation with UFT
-----------------------

This feature is available only in the *Premium* version of |squashAutom|.

In order to use this feature, an *OpenTestFactory* agent for *UFT* execution environment is needed.

1. Test reference
^^^^^^^^^^^^^^^^^

In order to bind a |squashTM| test case to a *UFT* automated test, the content of the *Automated test
reference* field of the *Automation* panel of a test case must have the following format |_| :

``[1]``

With :

* ``[1]`` : The path to the test folder to execute.

2. Nature of the exploitable Squash TM parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here is a table showing the exploitable parameters |_| :

.. csv-table::
   :header: "Nature","Key"
   :align: center

   "Name of the dataset","DSNAME"
   "Dataset parameter","DS_[name]"
   "Test case reference","TC_REF"
   "Test case CUF","TC_CUF_[code]"
   "Iteration CUF","IT_CUF_[code]"
   "Campaign CUF","CPG_CUF_[code]"
   "Test suite CUF","TS_CUF_[code]"

*Legend :*

* ``CUF`` : *Custom Field*
* ``[code]`` : *Value of a CUF's "Code" field*
* ``[name]`` : *Parameter name as filled in Squash TM*

3. Squash TM parameters usage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When executing a |squashTM| automated test case with *UFT*, it is possible to exploit the |squashTM| parameters inside
the test.

In order to achieve this, you'll have to follow these steps |_| :

* Create custom fields in |squashTM| and bind them to the project bearing the test plan to execute.

* Make sure that the *code* fields of the parameters correspond to the names of the existing parameters
  present in the *UFT* solution.

.. note:: |squashTM| adds a prefix to the *code* of the transmitted custom field. Make sure to take it into account.
          Please refer to the |squashTM|
          `documentation <https://sites.google.com/a/henix.fr/wiki-squash-tm/administrator-guide/05---custom-field-administration>`_
          for more information.

|

Below is an example of a *UFT* solution and the corresponding |squashTM| test case automation :

    .. container:: image-container

        .. image:: ../_static/autom/uft-test-with-params-1.png

    .. container:: image-container

        .. image:: ../_static/autom/uft-test-with-params-2.png

|

.. _execute_from_squash:

**********************************
Test plan execution from Squash TM
**********************************

Squash Orchestrator server declaration
======================================

In order to manually launch an execution plan from |squashTM|, the |squashOrchestrator| server that will execute the
automated tests in the suitable environments has to be declared. It is done in the *Automation servers* space of the
*Administration* |_| :

    .. container:: image-container

        .. image:: ../_static/autom/automation_servers_declaration.png

* ``Name`` : The name of server, as it will appear in the *Test Case* workspace.

* ``Type`` : Select *squashAutom* in the dropdown list.

* ``Url`` : The address of the |squashOrchestrator| *Receptionist*.

.. warning:: The |squashOrchestrator| *event bus* service **must** be accessible by the same url as the *Receptionnist*, on port 38368.

Once the server is created, you can set an authentication token.

    .. container:: image-container

        .. image:: ../_static/autom/token-en.png

.. note:: A token is mandatory for the execution of automated tests from |squashTM|.
          If the automation server does not require authentication token, you still have to set some value in |squashTM|.

Automated suite execution
=========================

Steps to run an automated test plan in |squashTM| are the usual ones:

* Get to the execution plan of the selected Iteration or Test Suite.

* Run the test using one of the button on the screen below :

    .. container:: image-container

        .. image:: ../_static/autom/test-plan-en.png

* An Overview of automated test executions popup shows up.

.. note:: The execution overview popup contains a new section displaying the ongoing executions performed by the
          |squashOrchestrator|. However, the state of the executions are not updated once launched  in the current
          version.

|

.. _result_publication:

*********************************************************
Published results after a Squash TM test plan execution
*********************************************************

Independently from the means used to trigger a test plan execution (from |squashTM| or a *Jenkins* pipeline), the kind
of results published in |squashTM| at the end of the execution of a test plan will differ depending on your using a
**Squash AUTOM Community** or **Squash AUTOM Premium** licence.

--------------------

Squash AUTOM Community
======================

After the execution of a |squashTM| test plan (iteration or test suite), the following information is updated |_| :

* ITPIs status update.

* Automated suite status update.

* The *Allure* type report containing all the results from the executed tests.

* The various ITPIs execution reports are accessible from the *Automated Suites* tab of the iteration or test suite |_| :

.. container:: image-container

    .. image:: ../_static/autom/automated-suite-tab.png

.. note:: All the results from the automated suite are compiled in an *Allure* type report, available in the list of
          reports as a *.tar* archive.

          For more information on the means to exploit and customize the *Allure* report, please refer to the
          `Allure documentation <https://docs.qameta.io/allure/>`_.

This, however, doesn't happen |_| :

* Creation of a new execution for each executed ITPI.

--------------------

Squash AUTOM Premium
====================

If you are using the **Squash AUTOM Premium** components, you have access to two types of results publication |_| :

* Light (default value).

* Full.

The choice of publication type is operated on a project basis by accessing the configuration of the
**Squash TM Result Publisher** plugin from the *Plugins* tab of your project page, inside the *Administration* Tab |_| :

    .. container:: image-container

        .. image:: ../_static/autom/plugin-complete-switch.png

|

Light results publication
-------------------------

By choosing the "Light" results publication, the following information is updated after the execution of a
|squashTM| test plan (iteration or test suite) |_| :

* ITPIs status update.

* Automated suite status update.

* The *Allure* type report containing all the results from the executed tests.

* The various ITPIs execution reports are accessible from the *Automated Suites* tab of the iteration or test suite |_| :

.. container:: image-container

    .. image:: ../_static/autom/automated-suite-tab.png

.. note:: All the results from the automated suite are compiled in an *Allure* type report, available in the list of
          reports as a *.tar* archive.

          For more information on the means to exploit and customize the *Allure* report, please refer to the
          `Allure documentation <https://docs.qameta.io/allure/>`_.

This, however, doesn't happen |_| :

* Creation of a new execution for each executed ITPI.

|

Full results publication
------------------------

By choosing the "Full" results publication, the following information is updated after the execution of a
|squashTM| test plan (iteration or test suite) |_| :

* ITPIs status update.

* Creation of a new execution for each executed ITPI.

* Automated suite status update.

* The *Allure* type report containing all the results from the executed tests.

* The execution reports of the various executions can be accessed from the *Automated Suites* tab of the iteration or
  test suite, or from the execution page (the reports are present in the attached files) |_| :

.. container:: image-container

    .. image:: ../_static/autom/iteration-execution-tab.png

.. container:: image-container

    .. image:: ../_static/autom/iteration-execution-detail.png

.. note:: All the results from the automated suite are compiled in an *Allure* type report, available in the list of
          reports as a *.tar* archive.

          For more information on the means to exploit and customize the *Allure* report, please refer to the
          `Allure documentation <https://docs.qameta.io/allure/>`_.

|

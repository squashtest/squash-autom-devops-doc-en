..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



###########################################################################
Piloting automated tests executions with an EPAC (Execution Plan «as code»)
###########################################################################

.. contents::
   :local:
   :depth: 1

|

|squashAutom| allows the redaction of an execution plan in a format specific to the |squashOrchestrator|, the EPAC
(Execution Plan «as code»), in order to orchestrate with precision the execution of automated tests outside of a test
repository.

You can find more information regarding the redaction of an EPAC in the |squashOrchestrator| documentation
(*Squash Orchestrator Documentation – 1.0.0.alpha2*, ``.pdf`` version) downloadable from
https://www.squashtest.com/community-download.

|

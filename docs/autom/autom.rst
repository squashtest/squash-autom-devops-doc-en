..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _autom:

############
Squash AUTOM
############

.. toctree::
   :hidden:
   :maxdepth: 2

   Installation<install.rst>
   Piloting automated tests executions with an EPAC<pilotWithPeac.rst>
   Piloting automated tests executions from Squash TM<pilotFromSquash.rst>

This guide will show you the various possibilities offered by the version *1.1.0.RELEASE* of |squashAutom|.

.. warning:: This version is intended to be used as a POC and therefore not in a production context (notably with a
             |squashTM| whose database is new or a copy of an existing one).

This *1.1.0.RELEASE* version provides the following components |_| :

* |squashOrchestrator| : it is a tool composed of a set of micro-services to be used by sending an execution plan written
  in a specific format, the EPAC (Execution plan «as code»), in order to orchestrate automated tests.

..

* **OpenTestFactory Agent** : agent to allow HTTP communication between a |squashOrchestrator| and a test execution environment.
  It is mandatory for Agilitest, Ranorex or UFT test execution.

..

* **Result Publisher Plugin for Squash TM** : this plugin for |squashTM| allows the return of information towards
  |squashTM| at the end of the execution of a |squashTM| execution plan by the |squashOrchestrator|.

..

* **Squash AUTOM Plugin for Squash TM** : this plugin for |squashTM| allows to execute automated test from |squashTM|
  with |squashOrchestrator|.

|

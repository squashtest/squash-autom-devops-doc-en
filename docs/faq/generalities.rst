..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _faq_generalities:

#######################################################
FAQ : Generalities about Squash AUTOM and Squash DEVOPS
#######################################################

This FAQ answers question about why develop these new products, what does this mean for **Squash TF** and how will the
transition from **Squash TF** to S|squashautom| and |squashdevops| take place?

.. contents::
   :local:
   :depth: 1


************************************************************
Why develop two new products: Squash AUTOM et Squash DEVOPS?
************************************************************

The creation of Squash AUTOM and Squash DEVOPS is the result of a reflection on the evolution of automation practices
(growth of CI/CD and DevOps practices, more and more democratized use of containerization, multiplication of integration tools)
and on how the Squash software suite could be in line with them.

It emerged that Squash TF had architectural and other limitations for its adoption within the DevOps principles.

This is why we decided to develop a new tool for managing the execution of automated tests, and this tool must respect
the following principles:

* Micro-service architecture, particularly for deployment and usability reasons in DevOps environments.

* Separation between the functionalities allowing automation (for testers and automation engineers) and those allowing
  the integration of automated tests (for the pipeline manager) within the DevOps plant.
  This gave rise to 2 products named Squash AUTOM and Squash DEVOPS.

* Removal of the adhesion with Squash TM in order to make these two products independent of it.

|

****************************************************
What is the model of Squash AUTOM and Squash DEVOPS?
****************************************************

The model chosen is an "open core" model.

This model, which is the same as Squash TM, allows to offer two versions:

* A free Community version composed of an open source core and freemium modules. This version is fully functional
  (unrestrained).

* A commercial version, with annual subscription, composed of the Community version and commercial plugins.
  It brings additional value-added features, but not essential, as well as support.

|

*************************************************************
Can Squash AUTOM and Squash DEVOPS be used without Squash TM?
*************************************************************

Yes.

For both products, our goal is to bring value to companies or projects that do not use Squash TM:

* The use of Squash AUTOM "alone" thus makes it possible to unify/homogenize the use of various automatons
  (Selenium, Cypress, SoapUI, Appium...) and various studios (Robot Framework, Cucumber, UFT, Agilitest...)
  while generating a common reporting format (such as Allure).

* The use of Squash DEVOPS "alone" enables the orchestration of all automated tests, their integration into the DevOps
  pipeline (CI/CD) and the posting of results to the recipients (the pipeline itself, the test asset tool or the
  framework for reporting and aggregating test results).

|

***********************************************************
Will new features for Squash TF be developed in the future?
***********************************************************

No, there will be no new features developed for Squash TF.

We encourage you to make the transition from Squash TF to Squash AUTOM for the execution of your automated test assets
in order to take advantage of all the new features offered by Squash.

Nevertheless, the elements of Squash TF will remain available for download.
Likewise, the open source repositories will remain accessible.

|

**********************************************************************************
Does support for Squash TF end with the release of Squash AUTOM and Squash DEVOPS?
**********************************************************************************

No.

We will continue to provide support on Squash TF via the Squashtest forum and, for customers of the Squash AUTOM
commercial offer, via our support service.

|

*******************************************************************************************************************
Does my legacy of automated tests, previously run with Squash TF, need to be modified to be used with Squash AUTOM?
*******************************************************************************************************************

No.

The scripts/automated tests that you run via Squash TF can be used by Squash AUTOM without any modification.

|

********************************************************
Can I run SKF tests with Squash AUTOM and Squash DEVOPS?
********************************************************

You will not be able to do it with the version 1.0.0.RELEASE.

SKF test support will be available in a later version before the end of Q2 2021.

|

*************************************************************************************************************
What do I need to do in Squash TM to run my automated execution plans with Squash AUTOM instead of Squash TF?
*************************************************************************************************************

It is necessary to create a link between a Squash TM test case and your automated test according to the Squash AUTOM documentation.

This action is almost instantaneous and can be done, at the same time, for several Squash TM Gherkin or BDD test cases
using the Git plugin. For the other test cases, an action on each test case will be necessary according to the Squash AUTOM
documentation.

The link action between a Squash TM test case and an automated test for an execution with Squash AUTOM is different
from the one for an execution with Squash TF.

|

************************************************************************************************************************
Can I mix in the same execution plan automated test cases executed by Squash TF and test cases executed by Squash AUTOM?
************************************************************************************************************************

Yes.

In order to ease the transition, it is perfectly possible to have test cases from a project running Squash TF and
test cases from a project running Squash AUTOM within the same Squash TM execution plan.

|

*********************************************************************************************
Do I need to have a Jenkins server to run my automated tests from Squash TM via Squash AUTOM?
*********************************************************************************************

No.

The specific Jenkins jobs required to run automated tests from Squash TM via Squash TF are no longer a prerequisite
for running from Squash TM via Squash AUTOM.

With Squash AUTOM, execution is handled by the Squash Orchestrator, a specific component of Squash AUTOM.

|

****************************************************************************************************************
Can I launch my Squash TM automated execution plans from a Jenkins pipeline with Squash AUTOM and SQUASH DEVOPS?
****************************************************************************************************************

Yes.

Running a Squash TM execution plan from a Jenkins pipeline is a new feature of Squash DEVOPS compared to Squash TF and
requires the implementation of jobs as described in the Squash DEVOPS documentation.

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



###################################################
Squash TM test execution plan retrieval with a PEAC
###################################################

.. contents::
   :local:
   :depth: 1

|

|squashDevops| gives you the possibility to retrieve an execution plan for automated tests defined in |squashTM| with
an EPAC. The EPAC can be triggered by a *Jenkins* pipeline (see the :ref:`corresponding page<call_from_jenkins>` of this
guide).

.. note:: To a proper functioning of this functionality, Test Plan Retriever plugin and Result Publisher plugin
          are required on targeted Squash TM.

|

*************
Prerequisites
*************

In order to retrieve an execution plan from |squashTM| with an EPAC, you need to perform the following tasks in
|squashTM| |_| :

* Create a user belonging to the *Test automation server* group.

* Create an execution plan (iteration or test suite) containing at least one ITPI linked to an automated test case, as
  described in the |squashAutom| user guide (see :ref:`here<automate_with_squash>`).

|

***********************************************************************
Integration of the Squash TM execution plan retrieval step into an EPAC
***********************************************************************

In order to retrieve an execution plan from |squashTM| with an EPAC, you need to call the corresponding *generator* action.

Here is a simple example of an EPAC in *Json* format allowing the retrieval of a |squashTM| execution plan |_| :

    .. code-block:: bash

        {
            "apiVersion": "opentestfactory.org/v1alpha1",
            "kind": "Workflow",
            "metadata": {
                "name": "Simple Workflow"
            },
            "defaults": {
                "runs-on":"ssh"
            },
            "jobs": {
                "explicitJob": {
                    "runs-on":"ssh",
                    "generator":"tm.squashtest.org/tm.generator@v1",
                    "with": {
                        "testPlanUuid":"1e2ae123-6b67-44b2-b229-274ea17ad489",
                        "testPlanType":"Iteration",
                        "squashTMUrl":"https://mySquashTMInstance.org/squash",
                        "squashTMAutomatedServerLogin":"tfserver",
                        "squashTMAutomatedServerPassword":"tfserver"
                    }
                }
            }
        }

A |squashTM| *generator* step must contain the following parameters |_| :

* ``testPlanType`` : Defines the type of test plan to retrieve in |squashTM|. Only the values *Iteration* and *TestSuite*
  are accepted.

..

* ``testPlanUuid`` : This is the UUID of the requested test plan. It can be found in the *Description* panel by clicking
  on the *Information* tab of the iteration or test suite in |squashTM|.

..

* ``squashTMUrl`` : URL of the targeted |squashTM|.

..

* ``squashTMAutomatedServerLogin`` : Name of the *Test automation server* group user to log into |squashTM|.

..

* ``squashTMAutomatedServerPassword`` : Password of the *Test automation server* group user to log into |squashTM|.

..

[*Optional fields*] |_| :

* ``tagLabel`` : Specific to the **Premium** version - It refers to the name of the *tag* type custom field on which the
  test cases to retrieve are to be filtered. It is not possible to specify more than one.

..

* ``tagValue`` : Specific to the **Premium** version - It refers to the value of the *tag* type custom field on which the
  test cases to retrieve are to be filtered. It is possible to specify multiple ones separated by "|"
  (*Example:* value1|value2). There has to be at least one value specified for the test case to be taken into account.

.. warning:: If one of the two *tagLabel* or *tagValue* fields is present, the other one **must** also be specified.

|

****************************************************
Squash TM parameters to exploit in an automated test
****************************************************

By executing an EPAC retrieving a |squashTM| execution plan, |squashTM| passes various pieces of information on ITPIs
that can be exploited in a *Cucumber*, *Cypress* or *Robot Framework* test case.

For more information, please refer to the :ref:`Squash TM parameters exploitation<squash_params>` section of the
|squashAutom| documentation, as well as the dedicated section on the desired automation framework.

|

***************************************************************
Results publication in Squash TM at the end of the execution
***************************************************************

The nature of the results published in Squash TM at the end of the execution will depend on the usage of a
**Squash AUTOM Community** or **Squash AUTOM Premium** licence.

Please refer to the |squashAutom| *1.0.0.alpha2* user guide for more information (see :ref:`here<result_publication>`).

|

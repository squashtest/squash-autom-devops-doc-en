..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _devops:

#############
Squash DEVOPS
#############

.. toctree::
   :hidden:
   :maxdepth: 2

   Installation<install.rst>
   Calling the Squash Orchestrator from a Jenkins pipeline<callFromJenkins.rst>
   Squash TM test execution plan retrieval with a PEAC<callWithPeac.rst>

This guide will show you the various possibilities offered by the version *1.1.0.RELEASE* of |squashDevops|.

.. warning:: This version is intended to be used as a POC and therefore not in a production context (notably with a
             |squashTM| whose database is new or a copy of an existing one).

This *1.0.0.RELEASE* version provides the following components |_| :

* **Squash TM Generator Micro-service for the Squash Orchestrator** : it is a micro-service for the |squashOrchestrator|
  allowing the retrieval of a |squashTM| test execution within an EPAC (Execution Plan «as code»). Please refer to the
  :ref:`Squash AUTOM<autom>` user guide for more information on the |squashOrchestrator| and the EPAC.

..

* **Test Plan Retriever for Squash TM** : this plugin for |squashTM| allows the sending of details about a |squashTM|
  execution plan to the |squashOrchestrator|.

..

* **Squash DEVOPS plugin for Jenkins** : this plugin for *Jenkins* facilitates the sending of an EPAC to the
  |squashOrchestrator| from a *Jenkins* pipeline.

|

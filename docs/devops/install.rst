..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



##################
Installation Guide
##################

.. contents::
   :local:
   :depth: 1

|

************************
Squash Generator Service
************************

Overview
============

The Squash Generator micro-service allows |squashTM| automated test plan retrieval when executing an EPaC with |squashOrchestrator|.

Installation
============

It is included in the*Docker* image of the |squashOrchestrator|.
For further details on |squashOrchestrator| installation, please refer to the :ref:`dedicated section<squash_orchestrator_installation>`.

To run |squashOrchestrator| Docker image with |squashDevops| **Premium** version of the Squash Generator Service, parameter
``-e SQUASH_LICENCE_TYPE=premium`` must be set in the ``docker run`` command of |squashOrchestrator|.

|

****************************************
Test Plan Retriever plugin for Squash TM
****************************************

Overview
============

This plugin allows |squashTM| automated test plan retrieval by a |squashOrchestrator| server.

Installation
============

The plugin exists in a **Community** version (*squash.tm.rest.test.plan.retriever.community-1.0.0.RELEASE.jar*) freely
available, or a **Premium** version (*squash.tm.rest.test.plan.retriever.premium-1.0.0.RELEASE.jar*) available on request.

For details on the installation, please refer to installation protocol of a |squashTM| plugin
(https://sites.google.com/a/henix.fr/wiki-squash-tm/installation-and-exploitation-guide/2---installation-of-squash-tm/7---jira-plug-in).

.. warning:: This plugin is compatible with version *1.22.2.RELEASE* or higher of |squashTM|.

|

*********************************
Squash DEVOPS plugin for Jenkins
*********************************

Overview
============

The plugin make calls to |squashOrchestrator| inside a pipeline easier.

Installation
============

The plugin is freely available from https://www.squashtest.com/community-download, as a ``.hpi`` file
(*squash-devops-1.1.0.hpi*).

To install it, you have to submit the plugin in the *Upload Plugin* area accessible by the *Advanced* tab of the *Plugin Manager* in
*Jenkins* configuration |_| :

.. container:: image-container

    .. image:: ../_static/devops/squash-devops-plugin-jenkins-upload.png

.. warning:: This plugin is compatible with version *2.164.1* or higher of *Jenkins*

|

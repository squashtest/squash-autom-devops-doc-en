..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


.. _call_from_jenkins:

#######################################################
Calling the Squash Orchestrator from a Jenkins pipeline
#######################################################

.. contents::
   :local:
   :depth: 1

|

********************************************
Configuring a Squash Orchestrator in Jenkins
********************************************

To access the configuration of the |squashOrchestrator|, you first need to go the *Configure System* page accessible in
the *System Configuration* space of *Jenkins*, through the *Manage Jenkins* tab |_| :

.. container:: image-container

    .. image:: ../_static/devops/jenkins-system-configuration.png

A panel named *Squash Orchestrator servers* will then be available |_| :

.. container:: image-container

    .. image:: ../_static/devops/jenkins-squash-orchestrator-server.png

* ``Server id`` : This ID is automatically generated and can't be modified. It is not used by the user.

..

* ``Server name`` : This name is defined by the user. It is the one that will be mentioned in the pipeline script of the
  workflow to be executed.

..

* ``Receptionist endpoint URL`` : The address of the *receptionist* micro-service of the orchestrator, with its port as
  defined at the launch of the orchestrator. Please refer to the |squashOrchestrator| documentation for further details.

..

* ``Workflow Status endpoint URL`` : The address of the *observer* micro-service of the orchestrator, with its port as
  defined at the launch of the orchestrator. Please refer to the |squashOrchestrator| documentation for further details.

..

* ``Credential`` : *Secret text* type *Jenkins* credential containing a *JWT Token* allowing authentication to the
  orchestrator. Please refer to the |squashOrchestrator| documentation for further details on secure access to the
  orchestrator.

..

* ``Workflow Status poll interval`` : This parameter sets the interval between each update of the workflow status.

..

* ``Workflow creation timeout`` : Timeout on the reception of the EPAC by the *receptionist* on the orchestrator side.

..

|

*******************************************************
Call to the Squash Orchestrator from a Jenkins pipeline
*******************************************************

Once there is at least one |squashOrchestrator| configured in *Jenkins*, it is possible to call the |squashOrchestrator|
from a *pipeline* type job in *Jenkins* thanks to a dedicated pipeline method.

Below is an example of a simple pipeline using the calling method to the orchestrator |_| :

    .. code-block:: bash

        node {
           stage 'Stage 1 : sanity check'
           echo 'OK pipelines work in the test instance'
           stage 'Stage 2 : steps check'
           configFileProvider([configFile(
        fileId: '600492a8-8312-44dc-ac18-b5d6d30857b4',
        targetLocation: 'testWorkflow.json'
        )]) {
           	def workflow_id = runSquashWorkflow(
               	workflowPathName:'testWorkflow.json',
               	workflowTimeout: '20S',
               	serverName:'defaultServer'
           	)
           	echo "We just ran The Squash Orchestrator workflow $workflow_id"
           }
        }

The *runSquashWorkflow* method allows the transmission of an EPAC to the orchestrator for an execution.

It uses 3 parameters |_| :

* ``workflowPathName`` : The path to the file containing the EPAC. In the present case, the file is injected through the
  *Config File Provider* plugin, but it is also possible to get it through other means (retrieval from a SCM, on the fly
  generation in a file, ...).

..

* ``workflowTimeout`` : Timeout on the actions execution. This timeout will be activated for example if an environment is
  unreachable (or doesn't exist), or if an action is not found by an actionProvider. It is to be adapted depending on
  the expected duration of the execution of the various tests in the EPAC.

..

* ``serverName`` : Name of the |squashOrchestrator| server to use. This name is defined in the *Squash Orchestrator servers*
  space of the *Jenkins* configuration.

|
